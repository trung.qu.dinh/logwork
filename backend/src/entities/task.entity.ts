import { ApiModelProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ProjectTask } from './project-task.entity';
import { TimeEntries } from './time-entries.entity';

@Entity({ name: 'tasks' })
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500 })
  @ApiModelProperty()
  name: string;

  @Column({
    name: 'created-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createAt: Date;

  @Column({
    name: 'updated-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  updateAt: Date;

  @Column({
    default: true
  })
  active: boolean;

  @OneToMany(type => ProjectTask, projectTask => projectTask.task)
  projectsTask: Promise<ProjectTask[]>;

  @OneToMany(type => TimeEntries, timeEntries => timeEntries.project)
  timeEntries: Promise<TimeEntries[]>;
}
