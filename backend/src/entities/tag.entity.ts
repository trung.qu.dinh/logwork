import { ApiModelProperty } from '@nestjs/swagger';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tags' })
export class Tag {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ length: 500 })
  @ApiModelProperty()
  name: string;

  @Column({
    name: 'created-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  createAt?: Date;

  @Column({
    name: 'updated-at',
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP'
  })
  updateAt?: Date;

  @Column({
    default: true
  })
  active?: boolean;
}
