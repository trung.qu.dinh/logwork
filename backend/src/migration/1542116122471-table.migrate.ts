import { MigrationInterface, QueryRunner } from 'typeorm';

export class migrate1542116122471 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS  `teams` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(500) NOT NULL, `description` varchar(500) NULL, `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `active` tinyint NOT NULL DEFAULT 1, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS  `users` (`id` int NOT NULL AUTO_INCREMENT, `full-name` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `user-name` varchar(255) NOT NULL, `avatar` varchar(255) NULL, `password` varchar(255) NOT NULL, `active` tinyint NOT NULL DEFAULT 1, `teamId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS `project-user` (`id` int NOT NULL AUTO_INCREMENT, `active` tinyint NOT NULL DEFAULT 1, `userId` int NULL, `projectId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS  `tasks` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(500) NOT NULL, `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `active` tinyint NOT NULL DEFAULT 1, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS `project-task` (`id` int NOT NULL AUTO_INCREMENT, `active` tinyint NOT NULL DEFAULT 1, `taskId` int NULL, `projectId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS `projects` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(500) NOT NULL, `created-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `updated-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'CREATE TABLE IF NOT EXISTS `time-entries` (`id` int NOT NULL AUTO_INCREMENT, `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `stop` datetime NULL, `lastStart` datetime NULL, `create-at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `duration` int NULL, `bill` int NULL, `active` tinyint NOT NULL DEFAULT 1, `isStop` tinyint NOT NULL DEFAULT 0, `taskId` int NULL, `projectId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB'
    );
    await queryRunner.query(
      'ALTER TABLE `users` ADD CONSTRAINT `FK_d1803064187c8f38e57a9c4984c` FOREIGN KEY (`teamId`) REFERENCES `teams`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `project-user` ADD CONSTRAINT `FK_7c36804f184eda128cba3978dd9` FOREIGN KEY (`userId`) REFERENCES `users`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `project-user` ADD CONSTRAINT `FK_d39ae16e4c3f08704ee8a49404a` FOREIGN KEY (`projectId`) REFERENCES `projects`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `project-task` ADD CONSTRAINT `FK_d6117337d5f04affd8acfa7a4a9` FOREIGN KEY (`taskId`) REFERENCES `tasks`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `project-task` ADD CONSTRAINT `FK_591dbf277b56eb1db89f878e135` FOREIGN KEY (`projectId`) REFERENCES `projects`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `time-entries` ADD CONSTRAINT `FK_cd24746bc152daf8dbd2f3a18ba` FOREIGN KEY (`taskId`) REFERENCES `tasks`(`id`)'
    );
    await queryRunner.query(
      'ALTER TABLE `time-entries` ADD CONSTRAINT `FK_f4cda2319a5a006d2b0b120efb2` FOREIGN KEY (`projectId`) REFERENCES `projects`(`id`)'
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      'ALTER TABLE `time-entries` DROP FOREIGN KEY `FK_f4cda2319a5a006d2b0b120efb2`'
    );
    await queryRunner.query(
      'ALTER TABLE `time-entries` DROP FOREIGN KEY `FK_cd24746bc152daf8dbd2f3a18ba`'
    );
    await queryRunner.query(
      'ALTER TABLE `project-task` DROP FOREIGN KEY `FK_591dbf277b56eb1db89f878e135`'
    );
    await queryRunner.query(
      'ALTER TABLE `project-task` DROP FOREIGN KEY `FK_d6117337d5f04affd8acfa7a4a9`'
    );
    await queryRunner.query(
      'ALTER TABLE `project-user` DROP FOREIGN KEY `FK_d39ae16e4c3f08704ee8a49404a`'
    );
    await queryRunner.query(
      'ALTER TABLE `project-user` DROP FOREIGN KEY `FK_7c36804f184eda128cba3978dd9`'
    );
    await queryRunner.query(
      'ALTER TABLE `users` DROP FOREIGN KEY `FK_d1803064187c8f38e57a9c4984c`'
    );
    await queryRunner.query('DROP TABLE `time-entries`');
    await queryRunner.query('DROP TABLE `projects`');
    await queryRunner.query('DROP TABLE `project-task`');
    await queryRunner.query('DROP TABLE `tasks`');
    await queryRunner.query('DROP TABLE `project-user`');
    await queryRunner.query('DROP TABLE `users`');
    await queryRunner.query('DROP TABLE `teams`');
  }
}
