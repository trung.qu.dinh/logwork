import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'modules/auth/auth.module';
import { ProjectModule } from 'modules/projects/project.module';
import { ReportModule } from 'modules/reports/report.module';
import { DashboardModule } from './modules/dashboard/dashboard.module'
import { TagModule } from 'modules/tag/tag.module';
import { TaskModule } from 'modules/task/task.module';
import { TeamModule } from 'modules/team/team.module';
import { TimerModule } from 'modules/timer/timer.module';
import { UserModule } from 'modules/user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule,
    UserModule,
    ProjectModule,
    TimerModule,
    ReportModule,
    DashboardModule,
    TeamModule,
    TaskModule,
    TagModule
  ]
})
export class AppModule {}
