import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeEntries } from 'entities/time-entries.entity';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
import { ReportController } from './controllers/report.controller';
import { ReportService } from './services/report.service';

@Module({
  imports: [TypeOrmModule.forFeature([TimeEntries, TimeEntriesRepository])],
  controllers: [ReportController],
  providers: [ReportService]
})
export class ReportModule {}
