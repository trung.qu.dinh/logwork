import { ApiModelProperty } from '@nestjs/swagger';
import { type } from 'os';

export class InputReportsSumary {
  @ApiModelProperty({ required: true })
  weekStart: number;
  @ApiModelProperty({ type: 'number', isArray: true })
  projects?: number[];
  @ApiModelProperty({ type: 'number', isArray: true })
  teams?: number[];
  @ApiModelProperty()
  searchTearm?: string;
}
