import { ApiModelProperty } from '@nestjs/swagger';

class TimeByProject {
  @ApiModelProperty()
  project: string;
  @ApiModelProperty()
  duration: number;
}

export class ReportTimerSumaryResponse {
  @ApiModelProperty()
  weekStart: number;

  @ApiModelProperty({ type: Number, isArray: true })
  totalTime: number[];

  @ApiModelProperty({ type: TimeByProject, isArray: true })
  getTimeByProjects: TimeByProject[];
}
