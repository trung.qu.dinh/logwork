import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TagRepository } from 'repositories/tag.repository';
import { TagInfo } from '../interfaces/tag.info.input';
@Injectable()
export class TagService {
  constructor(
    @InjectRepository(TagRepository)
    private readonly tagRepository: TagRepository
  ) {}

  public getTags() {
    return this.tagRepository.find();
  }

  public getTagsById(tagId: number) {
    return this.tagRepository.findOne({ id: tagId });
  }

  public create(tag: TagInfo) {
    return this.tagRepository.save(tag);
  }

  public update(id: number, tag: TagInfo) {
    return this.tagRepository.update(id, tag);
  }

  public delete(id: number) {
    return this.tagRepository.delete(id);
  }
}
