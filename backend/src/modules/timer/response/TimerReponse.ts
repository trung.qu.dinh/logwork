import { ApiModelProperty } from '@nestjs/swagger';
import Morphism from 'morphism';
import { ProjectResponse } from '../../projects/response/projectResponse';
import { TaskResponse } from '../../projects/response/TaskResponse';
import { compose, map, toPairs, sort } from 'ramda';
import * as moment from 'moment';
export class TimerResponse {
  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  start: string;

  @ApiModelProperty()
  stop: string;

  @ApiModelProperty()
  lastStart: string;
  @ApiModelProperty()
  createAt: string;
  @ApiModelProperty()
  isStop: boolean;
  @ApiModelProperty()
  duration: number;
  @ApiModelProperty({ type: ProjectResponse })
  project: ProjectResponse;
  @ApiModelProperty({ type: TaskResponse })
  task: TaskResponse;
}

const schemaTimer = {
  id: 'id',
  start: 'start',
  stop: 'stop',
  lastStart: 'lastStart',
  isStop: 'isStop',
  createAt: 'createAt',
  duration: data =>
    !!data.isStop ? moment(data.stop).unix() - moment(data.start).unix() : 0,
  project: 'project',
  task: 'task'
};

const mapperTimer = Morphism<TimerResponse>(schemaTimer);
export class GroupTaskTimerReponse {
  @ApiModelProperty()
  taskName: string;

  @ApiModelProperty({ type: TimerResponse, isArray: true })
  timers: TimerResponse[];
}

const schemaGroupTaskTimer = {
  taskName: data => data[0],
  timers: data => {
    return compose(
      map(mapperTimer),
      sort((timer1, timer2) => timer1.createAt < timer2.createAt)
    )(data[1]);
  }
};

const mapperGroupTaskTimer = Morphism<GroupTaskTimerReponse>(
  schemaGroupTaskTimer
);

export class GroupTimerReponse {
  @ApiModelProperty()
  date: string;

  @ApiModelProperty({ type: GroupTaskTimerReponse, isArray: true })
  tasksTimer: GroupTaskTimerReponse[];
}

const schemaGroupTimer = {
  date: data => data[0],
  tasksTimer: data => {
    return compose(
      mapperGroupTaskTimer,
      toPairs
    )(data[1]);
  }
};
const mapperGroupTimer = Morphism<GroupTimerReponse>(schemaGroupTimer);

export { mapperGroupTimer };
