import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeEntries } from 'entities/time-entries.entity';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
import { Project } from 'entities/project.entity';
import { ProjectRepository } from 'repositories/project.repository';
import { TimerController } from './controllers/timer.controller';
import { TimerService } from './services/timer.service';
import { Task } from 'entities/task.entity';
import { TaskRepository } from 'repositories/task.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TimeEntries,
      TimeEntriesRepository,
      Project,
      ProjectRepository,
      TaskRepository,
      Task
    ])
  ],
  controllers: [TimerController],
  providers: [TimerService]
})
export class TimerModule {}
