import {
    ApiUseTags,
    ApiResponse,
    ApiBearerAuth,
    ApiImplicitParam
  } from '@nestjs/swagger';
  import {
    Controller,
    Body,
    Post,
    Guard,
    UseGuards,
    Res,
    HttpStatus,
    Put,
    Param,
    Get,
    Delete
  } from '@nestjs/common';
  import { TaskService } from '../services/task.service';
  import { Task } from 'entities/task.entity';
  import { AuthGuard } from '@nestjs/passport';
  
  @ApiUseTags('task') 
  @Controller('task') 
  export class TaskController {
    constructor(private readonly taskService: TaskService) {}
    
    //Comment ten cua task làm truoc ham
    @Get('/:projectId')
    @ApiImplicitParam({ name: 'projectId' })
    @ApiBearerAuth() 
    @UseGuards(AuthGuard('jwt')) 
    @ApiResponse({ status: 201, description: 'Created' }) 
    public getTeamsOfProject(@Param('projectId') projectId) {
      return this.taskService.getTasksOfProject(projectId);
    }
  
    //Comment ten cua task làm truoc ham
    @Post() 
    @ApiBearerAuth() 
    @UseGuards(AuthGuard('jwt')) 
    @ApiResponse({ status: 201, description: 'Created' }) 
    public async createProject(
      @Res() res,
      @Body() task: Task
    ) {
      await this.taskService.create(task);
      res.status(HttpStatus.CREATED).send(); 
    }
//Select top 5 active task
    @Get('/Top5Task/')
  @ApiResponse({
    status: 200,
    description: 'Ok'
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async MostUser() {
    return await this.taskService.Top5Task();
  }
  
  }
  