import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Transactional } from 'typeorm-transactional-cls-hooked';
import { UserRepository } from 'repositories/user.repository';
import { ProjectUserRepository } from 'repositories/project-user.repository';
import { User } from 'entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository
  ) {}

  public async findAllUser() {
    return await this.userRepository.find();
  }

  @Transactional()
  public async create(user: User) {
    return await this.userRepository.save(user);
  }

  
  //show most active user
  public async MostUser() {
    const users = this.userRepository.createQueryBuilder('user')
    /*lấy ra user tham gia nhiều project nhất bằng cách:
    -Kết 2 bảng: user, project-user
    -đếm số project của mỗi user
    -lấy ra user có MAX(projects)
    */
    //return await this.userRepository.find();
  }
}
