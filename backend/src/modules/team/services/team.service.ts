import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TeamRepository } from 'repositories/team.repository';
import { Team } from 'entities/team.entity';
import { UserRepository } from 'repositories/user.repository';
@Injectable()
export class TeamService {
  constructor(
    @InjectRepository(TeamRepository)
    private readonly TeamRepository: TeamRepository,
    @InjectRepository(UserRepository)
    private readonly UserRepository: UserRepository
  ) {}

  // //ở đây cái input này có thhể là cái model hoặc 1 dto mà mình tự định
  // //create co them thong tin user nen o day them userid
  // public async createProject(userId: number, project: Project) {
  //   const user = await this.userRepository.findOne(userId);
  //   // oday dong thoi insert vao 2 bang nen phai mo transaction 1 bang la user va 1 bang la project
  //   return this.projectRepository.manager.transaction(
  //     async transactionManager => {
  //       // insert vao bang project
  //       console.log(project);

  //       await transactionManager.save(Project, project);
  //       //dong thoi phai insert vao bang user project
  //       //co the tao constructor roi nhet vao luon
  //       const newRecordUserProject = new ProjectUser();
  //       newRecordUserProject.user = user;
  //       newRecordUserProject.project = project;
  //       //sau do luu lai
  //       return transactionManager.save(newRecordUserProject);
  //     }
  //   );
  // }

  // public async updateProject(
  //   projectId: number,
  //   projectUpdate: InputUpdateProject
  // ) {
  //   const findProject = await this.projectRepository.findOne(projectId);
  //   return await this.projectRepository.update(
  //     { id: projectId },
  //     { ...findProject, ...projectUpdate }
  //   );
  // }

  // public async getUserOfProject(projectId: number) {
  //   const projectUser = await this.projectUserRepository.find({
  //     relations: ['user'],
  //     where: { projectId }
  //   });
  //   console.log(projectUser);
  //   return projectUser;
  // }

  //Show Team taken part in project
  public async getTeamsOfProject(projectId: number) {
    const TeamProject = await this.TeamRepository.createQueryBuilder('team')
      .innerJoin('team.users', 'users')
      .innerJoin('users.projectsUser', 'projectsUser')
      .where('projectsUser.projectId = :id')
      .setParameters({ id: projectId })
      .getMany();
    return TeamProject;
  }
  //add new team
  //@Transactional()
  public async create(team: Team) {
    return await this.TeamRepository.save(team);
  }

  //remove member
  public async RemoveMember(userId: number) {
    let UserUpdate = await this.UserRepository.findOne(userId);
    UserUpdate.team = null;
    return await this.UserRepository.save(UserUpdate);
  }

  //change permission
  public async ChangePermission(teamId: number) {
    console.log('Sua active cua team');
    let TeamUpdate = await this.TeamRepository.findOne(teamId);
    TeamUpdate.active = false;
    return await this.TeamRepository.save(TeamUpdate);
  }
}
