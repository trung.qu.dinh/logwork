import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectRepository } from '../../repositories/project.repository';
import { Project } from '../../entities/project.entity';
import { User } from 'entities/user.entity';
import { Team } from 'entities/team.entity';
import { TeamRepository } from 'repositories/team.repository';
import { UserRepository } from 'repositories/user.repository';
import { TeamController } from './controllers/team.controller';
import { TeamService } from './services/team.service';
import { ProjectUser } from 'entities/project-user.entity';
import { ProjectUserRepository } from 'repositories/project-user.repository';

//dang ky constroller va service
@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      ProjectRepository,
      User, // vi repository can 1 entity do minh dinh nghia o day nen dong thoi inject entity tuong ung
      UserRepository,
      ProjectUserRepository,
      ProjectUser,
      Team,
      TeamRepository
    ]) //cai import nay su dung de inject cac cai ma minh can, cu the o day trong file
    //service co su dung UserRepository, ProjectRepository nen phai inject vao
  ],
  controllers: [TeamController],
  providers: [TeamService]
})
export class TeamModule {}
//khi chay mode dev thi khi source thay doi no tu dong refresh lai
