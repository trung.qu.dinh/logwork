import { ApiModelProperty } from '@nestjs/swagger';
import Morphism from 'morphism';

export class ProjectResponse {
  @ApiModelProperty()
  description: string;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  createAt: string;

  @ApiModelProperty()
  updateAt: string;
}

const schemaProject = {
  description: 'description',
  name: 'name',
  createAt: 'createAt',
  updateAt: 'updateAt',
  id: 'id'
};

const mapperProject = Morphism<ProjectResponse>(schemaProject);
export { mapperProject };
