import { ApiModelProperty } from '@nestjs/swagger';
import Morphism from 'morphism';

export class TaskResponse {
  @ApiModelProperty()
  description: string;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  createAt: string;

  @ApiModelProperty()
  updateAt: string;
}

const schemaTask = {
  description: 'description',
  name: 'name',
  createAt: 'createAt',
  updateAt: 'updateAt'
};

const mapperTask = Morphism<TaskResponse>(schemaTask);
export { mapperTask };
