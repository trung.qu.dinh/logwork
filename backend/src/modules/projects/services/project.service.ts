import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from 'entities/project.entity';
import { TaskRepository } from 'repositories/task.repository';
import { ProjectTaskRepository } from 'repositories/project-task.repository';
import { Task } from 'entities/task.entity';
import { async } from 'rxjs/internal/scheduler/async';
import { ProjectTask } from 'entities/project-task.entity';
import { InputUpdateProject } from 'modules/projects/input/InputUpdateProject';
import { mapperProject } from 'modules/projects/response/projectResponse';
import { ProjectUserRepository } from 'repositories/project-user.repository';
import { ProjectRepository } from 'repositories/project.repository';
import { UserRepository } from 'repositories/user.repository';
import { Transactional } from 'typeorm-transactional-cls-hooked';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(ProjectRepository)
    private readonly projectRepository: ProjectRepository,
    @InjectRepository(ProjectUserRepository)
    private readonly projectUserRepository: ProjectUserRepository,
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    @InjectRepository(TaskRepository)
    private readonly taskRepository: TaskRepository,
    @InjectRepository(ProjectTaskRepository)
    private readonly projectTaskRepository: ProjectTaskRepository
  ) {}

  public async findAllOfUser(userId: number, name: string) {
    const projects = await this.projectRepository
      .createQueryBuilder('projects')
      .innerJoin('projects.usersProject', 'usersProject')
      .where('(:name IS NULL OR projects.name = :name)', {
        name
      })
      .getMany();
    return projects.map(mapperProject);
  }

  @Transactional()
  public async createProject(userId: number, project: Project) {
    project = await this.projectRepository.save(project);
    const user = await this.userRepository.findOne(userId);
    await this.projectUserRepository.save({ project, user });
    return true;
  }

  @Transactional()
  public async updateProject(
    projectId: number,
    projectUpdate: InputUpdateProject
  ) {
    const project = await this.projectRepository.findOne(projectId);
    if (!project) throw new NotFoundException('not found project');
    await this.projectRepository.save({ ...project, ...projectUpdate });
    return true;
  }

  //Show task in project
  @Transactional()
  public async getTasksOfProject(projectId: number) {
    const TeamProject = await this.taskRepository
      .createQueryBuilder('task')
      .innerJoin('task.projectsTask', 'projectsTask')
      .where('projectsTask.projectId = :id')
      .setParameters({ id: projectId })
      .getMany();
    return TeamProject;
  }

  //Create task in project
  public async createTaskInProject(projectId: number, task: Task) {
    const project = await this.projectRepository.findOne(projectId);
    return this.taskRepository.manager.transaction(async transactionManager => {
      await transactionManager.save(Task, task);
      const newRecoredProjectTask = new ProjectTask();
      newRecoredProjectTask.project = project;
      newRecoredProjectTask.task = task;

      return transactionManager.save(newRecoredProjectTask);
    });
  }

  //remove task in project
  public async removeTaskInProject(projectId: number, taskId: number) {
    const project = await this.projectRepository.findOne(projectId);
    const task = await this.taskRepository.findOne(taskId);
    console.log(project);
    console.log(task);
    let projectsTask = await this.projectTaskRepository.find({
      project: project,
      task: task
    });
    await this.projectTaskRepository.remove(projectsTask);
  }
}
