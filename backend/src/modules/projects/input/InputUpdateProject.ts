import { ApiModelProperty } from '@nestjs/swagger';

export class InputUpdateProject {
  @ApiModelProperty()
  name?: string;
  @ApiModelProperty()
  member?: number[];
}
