import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectRepository } from '../../repositories/project.repository';
import { Project } from '../../entities/project.entity';
import { ProjectController } from './controllers/project.controller';
import { ProjectService } from './services/project.service';
import { ProjectUser } from '../../entities/project-user.entity';
import { ProjectUserRepository } from '../../repositories/project-user.repository';
import { User } from 'entities/user.entity';
import { UserRepository } from 'repositories/user.repository';
import { Task } from 'entities/task.entity';
import { TaskRepository } from 'repositories/task.repository';
import { ProjectTaskRepository } from 'repositories/project-task.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      ProjectRepository,
      ProjectUser,
      ProjectUserRepository,
      User,
      UserRepository,
      Task,
      TaskRepository,
      ProjectTaskRepository
    ])
  ],
  controllers: [ProjectController],
  providers: [ProjectService]
})
export class ProjectModule {}
