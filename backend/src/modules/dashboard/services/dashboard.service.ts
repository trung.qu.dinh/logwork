import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeEntries } from 'entities/time-entries.entity';
import {
  compose,
  groupBy,
  head,
  map,
  merge,
  path,
  prop,
  reduce,
  values
} from 'ramda';
import { TimeEntriesRepository } from 'repositories/time-entries.repository';
import { InputDashboard } from '../input/input-dashboard';
import moment = require('moment');

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(TimeEntriesRepository)
    private readonly timeEntriesRepository: TimeEntriesRepository
  ) {}

  public async getReportSumary(queries: InputDashboard) {
    const timers = await this.getDataReport(queries);

    const getToTalTimeGroupByDate = this.getToTalTimeGroupByDate(
      queries.weekStart,
      timers
    );

    const mostActive = await this.getMostActive();

    const getTimeByProjects = compose(
      map(value => {
        return {
          projectName: compose(
            path(['project', 'name']),
            head
          )(value),
          duration: this.getTotalDuration(value)
        };
      }),
      values,
      groupBy(path(['project', 'id']))
    )(timers);
    return {
      weekStart: queries.weekStart,
      totalTime: getToTalTimeGroupByDate,
      getTimeByProjects,
      mostActive
    };
  }

  getTotalDuration(dataTineStamp) {
    return reduce((acc, value) => {
      return value.stop ? acc + moment(value.stop).diff(value.start) : acc;
    }, 0)(dataTineStamp);
  }

  getToTalTimeGroupByDate(weekStart, data) {
    const timeWeekStart = new Date(weekStart);

    const dayQuery = moment(timeWeekStart);
    const now = moment(timeWeekStart).add(6, 'days');
    const timersInitForDays = this.enumerateDaysBetweenDates(dayQuery, now);
    return compose(
      map(this.getTotalDuration),
      values,
      merge(timersInitForDays),
      groupBy((timer: TimeEntries) => {
        return moment(timer.createAt).format('M/D/YYYY');
      })
    )(data);
  }

  async getMostActive() {
      const userActive = await this.timeEntriesRepository.manager.query(
        'SELECT t.id, t.userName, t.fullName, t.duration as totalTime FROM (SELECT u.id, u.`user-name` as userName, u.`full-name` as fullName, SUM(t.stop - t.`start`) as duration FROM users u JOIN `time-entries` t ON u.id = t.userId GROUP BY u.id, t.stop, t.`start` ORDER BY duration DESC LIMIT 1) as t JOIN `project-user` p ON p.userId = t.id LIMIT 1;'
      );

      let activities = null
      if (userActive && userActive[0] !== undefined) {
          activities = await this.timeEntriesRepository.manager.query(
          'SELECT p.`name`, t.`name`, SUM(ti.stop - ti.`start`) as duration FROM `time-entries` ti JOIN tasks t ON t.id = ti.taskId JOIN projects p ON p.id = ti.projectId WHERE ti.userId = ' + userActive[0].id + ' GROUP BY p.`name`, t.`name`, duration;'
        )
      }

      let mostActive = null;

      if (userActive && userActive[0] !== undefined) {
        mostActive = {
          userName: userActive[0].userName,
          fullName: userActive[0].fullName,
          totalTime: parseInt(userActive[0].totalTime) * 1000,
          activities
        }
      }

      return mostActive;
  }

  async getDataReport(queries: InputDashboard) {
    const timeWeekStart = new Date(queries.weekStart);
    let timersQuery = this.timeEntriesRepository
      .createQueryBuilder('timeEntries')
      .leftJoinAndSelect('timeEntries.task', 'task')
      .leftJoinAndSelect('timeEntries.project', 'project')
      .leftJoinAndSelect('timeEntries.user', 'user')
      .leftJoin('user.team', 'team')
      .where(
        `(:startTime IS NULL OR (timeEntries.createAt >= :startTime AND timeEntries.createAt <= :endTime))
        AND timeEntries.isStop = 1`,
        {
          startTime: moment(timeWeekStart).toDate(),
          endTime: moment(timeWeekStart)
            .add(6, 'days')
            .toDate(),
        }
      );

    return timersQuery.orderBy('timeEntries.createAt', 'DESC').getMany();
  }

  enumerateDaysBetweenDates(startDate, endDate) {
    var now = startDate,
      dates = {};
    while (now.isSameOrBefore(endDate)) {
      dates[now.format('M/D/YYYY')] = [0];
      now.add(1, 'days');
    }
    return dates;
  }
}
