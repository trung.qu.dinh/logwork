import { Body, Controller, Post, UseGuards, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { InputDashboard } from '../input/input-dashboard';
import { DashboardResponse } from '../response/response-dashboard';
import { DashboardService } from '../services/dashboard.service';

@ApiUseTags('dashboard')
@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @Post()
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiResponse({
    status: 200,
    type: DashboardResponse,
    description: 'Return dashboard'
  })
  // @UseGuards(AuthGuard('jwt'))
  // @ApiBearerAuth()
  async reportSumaryForProject(@Body() queries: InputDashboard) {
    return await this.dashboardService.getReportSumary(queries);
  }
}
