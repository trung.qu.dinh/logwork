import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { ProjectUser } from 'entities/project-user.entity';

@EntityRepository(ProjectUser)
export class ProjectUserRepository extends BaseRepository<ProjectUser> {}
