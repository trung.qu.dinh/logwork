import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Team } from 'entities/team.entity';

@EntityRepository(Team)
export class TeamRepository extends BaseRepository<Team> {}
