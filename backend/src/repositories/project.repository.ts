import { EntityRepository } from 'typeorm';
import { BaseRepository } from 'typeorm-transactional-cls-hooked';
import { Project } from 'entities/project.entity';

@EntityRepository(Project)
export class ProjectRepository extends BaseRepository<Project> {}
