Vue.component('lw-timer', function (resolve, reject) {
  'use strict';
    $.get('./templates/timer.html')
    .then(function (template) {
      resolve({
          data (){
            return {
                projects: [
                  ],
                typetimes: {
                    "1":"today",
                    "2":"this week",
                  },
                datatable: [],
                counttime: "00:00:00",
                counttimer: 0,
                flagStart: true,
                StartStop: 'Start',
                slectproject: '',
                refreshIntervalId: null,
                hours : 0,
                minutes : 0,
                seconds : 0,
                Desoftimer: '',
            }
          },
          mounted () {
            var self = this;
            var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            var config = {
              headers: {
                Authorization: 'Bearer ' + accessToken
              }
            };
            // get project
            axios
            .get('/projects', config)
            .then(response => (
              response.data.forEach(function(element) {
                if(self.slectproject===''){
                  self.slectproject = element.id;
                }
                self.projects.push({key:element.id, value:element.name});
              })
              
              ))
            .catch(function (error) {
                  console.log('error get list projects ' +error);
                });
            // load data
            self.loadData();
          },
          methods : {
            StartStopTime : function() {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                projectId: self.slectproject,
                description: self.Desoftimer
              };
              // start  timer
              if(self.flagStart)
              {
                if(self.Desoftimer===''){
                  alert('please input description');
                  return;
                }
                axios
                .post('/timers/start', request, config)
                .then(function (response) {
                  console.log('start ok' +response.data);
                  self.loadData();
                })
                .catch(function (error) {
                  console.log('start fail ' +error);
                });
                self.flagStart =false;
                self.StartStop= 'Stop';
              }else{
                // stop timer
                request = {
                };
                axios
                  .post('/timers/stop', request, config)
                  .then(function (response) {
                    console.log('stop ok' +response.data);
                    self.loadData();
                  })
                  .catch(function (error) {
                    console.log('stop fail ' +error);
                  }); 
                  self.flagStart =true;
                  self.StartStop= 'Start';
              }
            },
            loadData: function(){
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var _startinterval =false;
              var request = {
                projectId: self.slectproject,
                description: self.Desoftimer
              };
              self.datatable = [];
              // get timer
              axios
              .get('/timers/', config)
              .then(response => {
                response.data.forEach(function(element) {
                  var _dt = element.date;
                  element.tasksTimer.forEach(function(tt){
                    var _timers = tt.timers;
                    self.datatable.push({id: '', Date: 'Group by', des: tt.taskName, counttime: 0, pj: ''});
                    var idextask = self.datatable.length;
                    var counttimertask ='0:0:0';
                    _timers.forEach(function(el) {
                      var coundt ='';
                      var _datestart = new Date(el.start);
                      var _datestartstr =_datestart.getDate()+'/'+_datestart.getMonth()+'/'+_datestart.getFullYear()
                      +' '+ _datestart.getHours()+':'+_datestart.getMinutes()+':'+_datestart.getSeconds();
  
                      if(el.isStop)
                      {
                        coundt = self.GetCountTime(el.start, el.stop);
                        var arr1 = coundt.split(":");
                        var arr2 = counttimertask.split(":");
                        counttimertask =self.Sumtime(Number(arr1[0]),Number(arr1[1]),Number(arr1[2]),Number(arr2[0]),Number(arr2[1]),Number(arr2[2]));
                      } else {
                        // which timer running
                        var counttimerrunning = self.GetCountTime(el.start, new Date());
                        var arr = counttimerrunning.split(":");
                        _startinterval =true;
                        self.StartInterval(arr[0],arr[1],arr[2]);
                        self.Desoftimer = tt.taskName;
                        self.slectproject = el.project.id;
                      }
                      self.datatable.push({id: el.id, Date: _datestartstr, des: tt.taskName, counttime: coundt, pj: el.project.name});
                    });
                    self.datatable[idextask -1].counttime = self.formatTimer(counttimertask);
                  })
                  // set time for run header
                });
                if(!_startinterval) {
                  self.StopInterval();
                  self.flagStart =true;
                  self.StartStop= 'Start';
                }else{
                  self.flagStart =false;
                  self.StartStop= 'Stop';
                }
              })
              .catch(function (error) {
                    console.log('error get list timer ' +error);
                  });
            },
            GetCountTime: function(dt1, dt2)
            {
              var self = this;
              var coundt ='';
              var date1 = new Date(dt1);
              var date2 = new Date(dt2);
              // get total seconds between the times
              var delta = Math.abs(date2 - date1) / 1000;
              
              // calculate (and subtract) whole days
              var days = Math.floor(delta / 86400);
              delta -= days * 86400;
              // calculate (and subtract) whole hours
              var hours = Math.floor(delta / 3600) % 24;
              delta -= hours * 3600;
              coundt =hours;
              // calculate (and subtract) whole minutes
              var minutes = Math.floor(delta / 60) % 60;
              delta -= minutes * 60;
              coundt +=':'+minutes;
              // what's left is seconds
              var seconds = delta % 60;
              coundt +=':'+Math.round(seconds);
              return self.formatTimer(coundt);
            },

            StartInterval: function(hours, minutes, seconds) {
              if(hours=== undefined || minutes=== undefined || seconds=== undefined){
                return;
              }
              console.log('StartInterval ');
              var self = this;
              self.hours =hours;
              self.minutes =minutes;
              self.seconds =seconds;
              self.refreshIntervalId = setInterval(() =>{
                self.seconds++;
                if(self.seconds>59){
                  self.minutes++;
                  if(self.minutes>59){
                    self.hours++;
                    self.minutes=0;
                  }
                  self.seconds=0;
                } 
                self.counttime = self.formatTimer(self.hours+ ':' + self.minutes + ':'+self.seconds);
              }, 1000);
            },
            Sumtime: function(hours, minutes, seconds, hours1, minutes1, seconds1) {
              if(hours=== undefined || minutes=== undefined || seconds=== undefined){
                return;
              }
              var self = this;
              var str =self.formatTime(self.timestrToSec(hours+ ':' + minutes + ':'+ seconds) + self.timestrToSec(hours1+ ':' + minutes1 + ':'+ seconds1));
              //(hours+ ':' + minutes + ':'+ seconds, hours1+ ':' + minutes1 + ':'+ seconds1)
              return str;
            },
            timestrToSec: function (timestr) {
              var parts = timestr.split(":");
              return (parts[0] * 3600) +
                     (parts[1] * 60) +
                     (+parts[2]);
            },
            
            pad: function (num) {
              if(num < 10) {
                return "0" + num;
              } else {
                return "" + num;
              }
            },
            
            formatTime: function (seconds) {
              var self = this;
              return [self.pad(Math.floor(seconds/3600)%60),
                self.pad(Math.floor(seconds/60)%60),
                self.pad(seconds%60),
                      ].join(":");
            },

            StopInterval: function() {
              console.log('StopInterval ');
              var self = this;
              if(self.refreshIntervalId=== undefined){ return;}
              self.counttime = "00:00:00";
                self.hours = 0;
                self.minutes = 0;
                self.seconds = 0;
                clearInterval(self.refreshIntervalId);
            },
            SelectRow: function(item) {
              var self = this;
              if(self.flagStart === true){
                self.Desoftimer =item.des;
              }
            },
            formatTimer: function(stimer) {
              var arr = stimer.split(':');
              var str='';
              if(arr[0].length <2)
              {
                arr[0]= 0 +arr[0];
                str= arr[0] +':';
              }
              else{ str = arr[0] +':'; }
              if(arr[1].length <2)
              {
                arr[1]= 0 +arr[1];
                str += arr[1] +':';
              }
              else{ str += arr[1] +':'; }

              if(arr[2].length <2)
              {
                arr[2]= 0 +arr[2];
                str += arr[2];
              }
              else{ str += arr[2]; }
              
              return str;
            }
         }, 
          template: template})
    });

  });