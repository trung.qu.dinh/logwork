(function (console, axios, _) {
  'use strict';

  new Vue({
    el: '#lw-login',
    data: {
      loginUsername: '',
      loginPassword: '',
      loginShowPwd: false,
      loginFailed: false,
      loginSuccess: false,
      loginErrorMsg: '',
      loginSuccessMsg: '',

      registerUsername: '',
      registerPassword: '',
      confirmPassword: '',
      registerFullName: '',
      email: '',
      registerShowPwd: false,
      showConfirmPassword: false,
      registerFailed: false,
      registerSuccess: false,
      uploadedAvatar: '',
      isLogin: true
    },
    methods: {
      login: function () {
        var self = this;
        self.loginSuccess = false;
        self.loginFailed = false;

        if (self.loginUsername === '' || self.loginPassword === '') {
          return;
        }

        axios
          .post('/auth/login', {
            userName: self.loginUsername,
            password: self.loginPassword
          })
          .then(function (response) {
            var token = _.get(response, 'data.access_token', '');
            var userName = _.get(response, 'data.userName', null);
            var fullName = _.get(response, 'data.fullName', '');
            var email = _.get(response, 'data.email');
            var avatar = 'img/' + (_.get(response, 'data.avatar') || 'default') + '.jpg';

            document.cookie = 'accessToken=' + token;
            document.cookie = 'userName=' + userName;
            document.cookie = 'fullName=' + fullName;
            document.cookie = 'email=' + email;
            document.cookie = 'avatar=' + avatar;

            window.location.href = '/login';

            self.loginSuccess = true;
            self.successMsg = 'User logged in successfully!'
          })
          .catch(function (error) {
            if (_.get(error, 'response.status') === 400) {
              self.loginFailed = true;
              self.errorMsg = _.get(error, 'response.data.message');
            }
          });
      },
      register: function () {
        var self = this;
        self.registerSuccess = false;
        self.registerFailed = false;

        if (self.registerUsername === '' || self.registerPassword === '') {
          return;
        }

        /*{
         "fullName": "string",
         "email": "string",
         "userName": "string",
         "avatar": "string",
         "password": "string"
         }*/

        axios
          .post('/auth/register', {
            fullName: self.registerFullName,
            userName: self.registerUsername,
            password: self.registerPassword,
            email: self.email,
            avatar: self.uploadedAvatar
          })
          .then(function (response) {
            self.registerFullName = '';
            self.registerUsername = '';
            self.registerPassword = '';
            self.email = '';
            self.uploadedAvatar = '';

            self.isLogin = true;

            self.registerSuccess = true;
            self.successMsg = 'User registered successfully, please log in!'
          })
          .catch(function (error) {
            if (_.get(error, 'response.status') === 400) {
              self.registerFailed = true;
              self.errorMsg = _.get(error, 'response.data.message');
            }
          });
      }
    },
    computed: {
      inputType: function () {
        var self = this;
        return self.loginShowPwd ? 'text' : 'password';
      },
      checkLoginDisabled: function () {
        var self = this;
        return (self.loginUsername === '' || self.loginPassword === '');
      },
      checkRegisterDisabled: function () {
        var self = this;
        return (self.registerUsername === '' || self.registerPassword === '' || !self.isValidEmail
        || self.confirmPassword === '' || self.registerPassword !== self.confirmPassword);
      },
      isValidEmail: function () {
        var self = this;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(self.email.toLowerCase());
      }
    }
  })

})(window.console, window.axios, window._);