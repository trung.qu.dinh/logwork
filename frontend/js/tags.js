(function (axios, _) {
  'use strict';

  Vue.component('lw-tags', function (resolve, reject) {
    axios
      .get('./templates/tags.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              tags: [],
              newTag: '',
              selectedTag: ''
            }
          },
          methods: {
            getData: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              axios
                .get('/tag', config)
                .then(function (response) {
                  var resData = _.get(response, 'data');

                  self.tags = _.cloneDeep(resData);
                })
                .catch(function (error) {

                });
            },
            addTag: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                name: self.newTag
              };

              axios
                .post('/tag', request, config)
                .then(function () {
                  self.newTag = '';

                  self.getData();
                })
                .catch(function (error) {

                });
            },
            pickTag: function (tag) {
              var self = this;
              self.selectedTag = tag;
            },
            editTag: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                name: self.selectedTag.name
              };

              axios
                .put('/tag/' + self.selectedTag.id, request, config)
                .then(function () {
                  self.selectedTag = {};
                  self.getData();
                })
                .catch(function (error) {

                });
            },
            deleteTag: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };

              axios
                .delete('/tag/' + self.selectedTag.id, config)
                .then(function () {
                  self.selectedTag = {};
                  self.getData();
                })
                .catch(function (error) {

                });
            }
          },
          mounted: function () {
            var self = this;
            self.getData();
          },
          template: _.get(template, 'data')
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

})(window.axios, window._);