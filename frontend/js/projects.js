(function (axios, _, moment) {
  'use strict';

  Vue.component('lw-projects', function (resolve, reject) {
    axios
      .get('./templates/projects.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              projects: [],
              sortBy: '',
              sortOrder: '',
              searchKey: '',
              newProjectName: ''
            }
          },
          methods: {
            getData: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              axios
                .get('/projects', config)
                .then(function (response) {
                  var resData = _.get(response, 'data');
                  var duration = 0;

                  self.projects = _.cloneDeep(resData);

                  _.forEach(self.projects, function (proj) {
                    duration = moment(proj.updateAt) - moment(proj.createAt);
                    proj.timeSpent = moment.utc(moment.duration(duration).asMilliseconds()).format('HH:mm:ss');
                  });

                  self.projectsSorted = _.cloneDeep(self.projects);
                })
                .catch(function (error) {

                });
            },
            sortTable: function (sortKey) {
              var self = this;
              if (self.sortBy === sortKey) {
                self.sortOrder = (self.sortOrder === 'asc' ? 'desc' : (self.sortOrder === 'desc' ? '' : 'asc'));
              } else {
                self.sortOrder = 'asc';
                self.sortBy = sortKey;
              }
            },
            createProject: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                name: self.newProjectName
              };

              axios
                .post('/projects', request, config)
                .then(function (response) {
                  self.newProjectName = '';

                  self.getData();
                })
                .catch(function (error) {

                });
            }
          },
          computed: {
            projectsSorted: function () {
              var self = this;
              var matchedItem = [];

              if (self.searchKey !== '') {
                matchedItem = _.filter(self.projects, function (proj) {
                  return proj.name.indexOf(self.searchKey) >= 0
                    || self.$options.filters.convertDate(proj.createAt).indexOf(self.searchKey) >= 0
                    || self.$options.filters.convertDate(proj.updateAt).indexOf(self.searchKey) >= 0;
                });
              } else {
                matchedItem = _.cloneDeep(self.projects);
              }

              if (!self.sortOrder) {
                self.sortBy = '';
                return matchedItem;
              }
              return _.orderBy(matchedItem, self.sortBy, self.sortOrder);
            },
            canCreateProject: function () {
              var self = this;
              return !(_.isString(self.newProjectName) && self.newProjectName !== '');
            }
          },
          mounted: function () {
            var self = this;
            self.getData();
          },
          filters: {
            convertDate: function (value) {
              return moment(value).format('HH:mm:ss DD/MM/YYYY');
            }
          },
          template: _.get(template, 'data')
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

})(window.axios, window._, window.moment);