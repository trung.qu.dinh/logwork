(function (axios, _, moment, Chart, $) {
  'use strict';

  function getColorCode() {
    return ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850'];
  }

  function convertDuration(duration, format) {
    return moment.utc(moment.duration(duration).asMilliseconds()).format(format);
  }

  function generateBartChartConfig() {
    return {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              callback: function (value) {
                return convertDuration(value, 'HH') + ' h';
              }
            }
          }
        ]
      },
      tooltips: {
        callbacks: {
          title: function (tooltipItem, data) {
            return data['labels'][tooltipItem[0]['index']][1];
          },
          label: function (tooltipItem, data) {
            var label = data['datasets'][0].label;
            var duration = data['datasets'][0]['data'][tooltipItem['index']] || "";
            if (duration) {
              return label + convertDuration(duration, 'HH:mm:ss');
            }
          }
        }
      }
    };
  }

  function generatePieChartConfig() {
    return {
      legend: {
        display: true,
        position: 'top',
      },
      elements: {
        center: {
          color: '#000000',
          fontStyle: 'Arial',
          sidePadding: 20
        }
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            var label = data['labels'][[tooltipItem['index']]];
            if (label) {
              var duration = data['datasets'][0]['data'][tooltipItem['index']];
              label += ': ';
              label += convertDuration(duration, 'HH:mm:ss');
            }
            return label;
          }
        }
      }
    };
  }

  function updateBarChart(self, data) {
    var weekStart = _.get(data, 'weekStart', 0);
    var totalTime = _.get(data, 'totalTime', []);
    var totalDuration = 0;

    self.barChartData.data.labels.length = 0;
    self.barChartData.data.labels.push(['Sun', moment(weekStart).add(0, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Mon', moment(weekStart).add(1, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Tue', moment(weekStart).add(2, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Wed', moment(weekStart).add(3, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Thu', moment(weekStart).add(4, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Fri', moment(weekStart).add(5, 'days').format('DD/MM/YYYY')]);
    self.barChartData.data.labels.push(['Sat', moment(weekStart).add(6, 'days').format('DD/MM/YYYY')]);

    self.barChartData.data.datasets[0].data = totalTime;

    _.forEach(totalTime, function (time) {
      totalDuration = totalDuration + time;
    });

    self.totalTimeSpent = convertDuration(totalDuration, 'HH:mm:ss');

    self.barChart.update();
  }

  function updatePieChart(self, data) {
    var weekStart = _.get(data, 'weekStart', 0);
    var timeByProject = _.get(data, 'getTimeByProjects', []);
    var totalDuration = 0;

    self.pieChartData.data.labels.length = 0;
    self.pieChartData.data.datasets[0].data.length = 0;

    _.forEach(timeByProject, function (obj) {
      self.pieChartData.data.labels.push(obj.projectName);
      self.pieChartData.data.datasets[0].data.push(obj.duration);
      totalDuration = totalDuration + obj.duration;
    });

    self.pieChart.update();

    self.pieChartData.centerText.text = convertDuration(totalDuration, 'HH:mm') + ' h';
  }

  function updatePanel(self, data) {
    self.userName = _.get(data, 'mostActive.userName', '');
    self.fullName = _.get(data, 'mostActive.fullName', '');
    self.totalTime = _.get(data, 'mostActive.totalTime', '');
    self.activity = _.get(data, 'mostActive.activities', [])
  }

  Vue.component('lw-dashboard', function (resolve, reject) {
    axios
      .get('./templates/dashboard.html')
      .then(function (template) {
        resolve({
          data: function () {
            return {
              barChartId: 'bar-chart',
              barChartData: {
                type: 'bar',
                data: {
                  labels: [],
                  datasets: [
                    {
                      data: [],
                      label: 'Total Time: ',
                      backgroundColor: getColorCode(),
                    }
                  ]
                },
                options: generateBartChartConfig()
              },
              pieChartId: 'dashboard-pie-chart',
              pieChartData: {
                type: 'doughnut',
                data: {
                  labels: [],
                  datasets: [
                    {
                      data: [],
                      backgroundColor: getColorCode()
                    }
                  ]
                },
                options: generatePieChartConfig(),
                centerText: {
                  display: true
                }
              },
              totalTimeSpent: 0,
              pickerSelector: '#weekPickerDashboard',
              weekDate: '',
              currentDate: 0,
              weekStart: 0,
              userName: '',
              fullName: '',
              totalTime: 0,
              activity: []
            }
          },
          methods: {
            getData: function () {
              var self = this;
              var accessToken = document.cookie.replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
              var config = {
                headers: {
                  Authorization: 'Bearer ' + accessToken
                }
              };
              var request = {
                weekStart: self.weekStart
              };
              axios
                .post('/dashboard', request, config)
                .then(function (response) {
                  var resData = _.get(response, 'data');

                  if (!resData) {
                    return;
                  }

                  updateBarChart(self, resData);
                  updatePieChart(self, resData);
                  updatePanel(self, resData);
                })
                .catch(function (error) {

                });
            },
            showPicker: function () {
              var self = this;
              $(self.pickerSelector).datepicker('show');
            },
            applyFilter: function () {
              var self = this;

              self.calculateWeekDate();
              self.getData();
            },
            calculateWeekDate: function () {
              var self = this;
              var dateFormat = 'DD/MM/YYYY';

              self.currentDate = self.currentDate || moment(new Date()).format(dateFormat);
              self.weekStart = moment(self.currentDate, dateFormat).day(0).valueOf();

              var firstDate = moment(self.currentDate, dateFormat).day(0).format(dateFormat);
              var lastDate = moment(self.currentDate, dateFormat).day(6).format(dateFormat);

              self.weekDate = firstDate + ' - ' + lastDate;

              self.$forceUpdate();
            }
          },
          computed: {
            avatar: function () {
              var self = this;
              return 'img/' + self.userName + '.jpg';
            }
          },
          mounted: function () {
            var self = this;

            var barChartCtx = document.getElementById(self.barChartId);
            self.barChart = new Chart(barChartCtx, self.barChartData);

            var pieChartCtx = document.getElementById(self.pieChartId).getContext('2d');
            self.pieChart = new Chart(pieChartCtx, self.pieChartData);

            var weekPicker = $(self.pickerSelector);

            weekPicker.datepicker({
              format: 'dd/mm/yyyy',
              autoclose: true,
              orientation: 'right',
              todayBtn: 'linked',
              todayHighlight: true
            });

            weekPicker.on('hide', function () {
              self.currentDate = weekPicker.val();
              self.calculateWeekDate();
              self.getData();
            });

            self.calculateWeekDate();
            self.getData();
          },
          filters: {
            convertDuration: function (value) {
              return convertDuration(value, 'HH:mm:ss');
            }
          },
          template: _.get(template, 'data')
        });
      })
      .catch(function (error) {
        reject('ERROR: ', error);
      });
  });

})(window.axios, window._, window.moment, window.Chart, window.jQuery);