(function () {
  'use strict';

  var express = require('express');
  var proxy = require('http-proxy-middleware');
  var _ = require('lodash');
  var proxyConfig = require('../config.json');
  var app = express();
  var rootDir = __dirname + '/';
  var UI_PORT = _.get(proxyConfig, 'frontend.port', 41206);
  var SERVER_PORT = _.get(proxyConfig, 'backend.port', 3000);
  var SERVER_URL = 'http://localhost:' + SERVER_PORT;
  var CONTEXT = ['/auth', '/projects', '/timers', '/reports', '/users', '/tag', '/dashboard'];

  var proxyOptions = {
    target: SERVER_URL,
    changeOrigin: true,
    ws: true,
    logLevel: 'debug',
    secure: false
  };
  var wsProxy = proxy(CONTEXT, proxyOptions);

  function redirectToLogin(req, res, next) {
    if (!req.get('Cookie') || !req.get('Cookie').replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1")) {
      res.redirect('/login');
    } else {
      next();
    }
  }

  function redirectToIndex(req, res, next) {
    if (req.get('Cookie') && req.get('Cookie').replace(/(?:(?:^|.*;\s*)accessToken\s*\=\s*([^;]*).*$)|^.*$/, "$1")) {
      res.redirect('/');
    } else {
      next();
    }
  }

  app.use(wsProxy);
  app.use(express.static(rootDir, {index: false}));

  app.get('/login', redirectToIndex, function (req, res) {
    res.sendFile('login.html', {root: __dirname + '/'});
  });

  app.get('/', redirectToLogin, function (req, res) {
    res.sendFile('index.html', {root: __dirname + '/'});
  });

  app.listen(UI_PORT, function () {
    console.log('Server running at http://localhost:' + UI_PORT + '/');
  });

  require('opn')('http://localhost:' + UI_PORT + '/login');

})();