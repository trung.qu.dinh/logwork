
## Rủi ro

1.  Thành viên trong team bỏ ngang không thực hiện dự án.
    - Hành động dự kiến:
        - Tính toán lại thời gian còn lại và mức độ hoàn thành của dự án
        - Chọn ra những task có độ ưu tiên thấp để lượt bỏ.
        - Thông báo với giáo viên phụ trách.
2.  Release không đủ feature đáp ứng
    - Báo cáo phần mềm đã fail.
	- Công bố những tính năng đã làm được.
	- Xác định nếu có thể thì cần thêm bao nhiêu thời gian để có thể hoàn thành đủ feature.
    - Tạch
3.  Xung đột cách làm việc giữa các thành viên
    - Cần sự gặp nhau trực tiếp giữa các thành viên.
    - Dựa trên quy trình mà thực hiện, lúc này mọi thống nhất sẽ là do team quyết định.
4.  Thời gian thống nhất cuộc họp giữa các thành viên
    - ScM sẽ nhắc nhở và tích cực thông báo trên các kênh.
	- ScM trực tiếp làm việc với từng thành viên để cập nhật tình hình và đưa ra giải pháp để đảm bảo tiến độ
5.  Hạn chế về mặt giao tiếp khiến khó có thể theo dõi tiến độ
    - ScM làm việc với từng thành viên
	- Sử dụng kênh thông báo chung
	- Thiết lập deadline và thường xuyên nhắc nhở
6.  Hạn chế về kĩ thuật
    - Lượt bỏ những task yêu cầu kĩ thuật nhưng không quan trọng.
    - Nếu đó là task không thể bỏ, lượt bỏ những task khách để tập trung vào task cần thiết
7.  Thành viên không chủ động trong khâu làm việc
	- Đối với thành viên không chủ động, assign task có độ ưu tiên thấp hơn
8.  Thành viên không nắm rõ quy trình
    - Phổ biến lại quy trình cho thành viên.
9. Phát hiện sai kiến trúc sau 1 thời gian hiện thực hóa phần mềm
    - Thiết lập buổi gặp mặt trực tiếp để tập trung giải quyết vấn đề
	- Sử dụng lợi thế khi các task được chia nhỏ hết mức có thể, từ đó giúp thời gian thay đổi kiến trúc được giảm thiểu.
10.  Quy trình bị thắt cổ chai tại 1 giai đoạn
    - Xác định khu vực thắt cổ chai
	- Tâp trung đủ nhân lực, lúc nào các thành viên đó phải chấp nhận bỏ dở task đang làm để giải quyết khu vực thắt cổ chai.