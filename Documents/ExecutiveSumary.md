## Executive summary

Sản phẩm “Website quản lý thời gian LogWork “ là trang web giải quyết vấn đề về việc quản lý thời gian của các cá nhân, tổ chức muốn ghi lại quá trình làm việc (thời gian, đặc điểm công việc,…) đồng thời đưa ra giải pháp chi việc thống kê thông tin của các tác vụ, giúp người dùng nhận xét được quá trình làm việc của bản thân, tổ chức.
Sản phẩm nhắm đến thị trường là những cá nhân, tổ chức, cơ quan có nhu cầu quản lý thời gian làm việc.

- Vấn đề:
    - Trong bối cảnh làm việc hiện tại của các công ty, việc theo dõi tiến độ thực hiện 1 dự án và đưa ra giải pháp kịp thời để đảm bảo dự án hiện tại và sau này thành công là vô cùng cần thiết. Một trong các góp phần vào là ghi lại thời gian, ghi chú thông tin các tác vụ trong quá trình làm việc. Hiện tại, một số công ty chỉ thực hiện việc quản lý thời gian này chỉ dừng lại ở mức đơn giản bằng cách ghi chú lại nhưng không có tổ chức rõ ràng. Khi có được bản ghi chú này, nhà quản lý lại phải đối mặt với việc tổng hợp, thống kê lại thông tin của các tác vụ. Điều này gây mất rất nhiều thời gian hoàn thành.
	
- Giải pháp:
    - Đưa ra một giải pháp quản lý thời gian toàn diện, qua đó dễ dàng theo dõi tiến độ công việc.
    - Lưu trữ thông tin các tác vụ nhanh chóng, cung cấp giao diện dễ sử dụng thay cho quá trình ghi chép tác vụ bằng tay mất nhiều thời gian.
    - Cung cấp các công cụ thống kê tác vụ nhằm đưa ra những hình thức báo cáo giúp người dùng có thể nhận xét quá trình làm việc của bản thân, tổ chức.
    - Ứng dụng xây dựng trên nền tảng web để người dùng có thể truy cập từ bất cứ đâu, thuận tiện, nhanh chóng, chính xác.

- Nhóm đối tượng tập trung:
    - Các công ty doanh nghiệp quản lý theo 1 quy trình nào đó.
	- Các team đông người cần 1 công cụ kiểm soát nhóm.
	- Các công ty start-up muốn theo dõi, phân tích tiến độ.
	- Các cá nhân muốn ghi lại quá trình làm việc của mình.
	
- Các đối thủ cạnh tranh:
    - toggl.com
    - track.timeneye.com
    - tmetric.com
	- ...

- Rủi ro
    - Hệ thống không thể đáp ứng được tất cả nghiệp vụ khác nhau.
	- Chi phí đầu tư thấp về mặt thời gian, kinh phí, nhân lực, ... dẫn đến khó cạnh tranh trên thị trường.
    - Thời gian không đủ để hoàn thiện sản phẩm, khó bắt kịp được các đối thủ cạnh tranh.
	- Công nghệ thực hiện dựa trên open source nên phải chấp nhận rủi ro nếu bên cung cấp công nghệ có vấn đề.
    - Sử dụng công nghệ mới để phát triển, nên rủi ro khá cao.

- Cơ hội:
    - Vẫn đủ khả năng cạnh tranh nếu hoàn thành tốt các chức năng cần thiết cơ bản.
	- Có lợi thế dựa trên sự tham khảo, hệ thống có UX/UI thân thiện hơn với người dùng.
